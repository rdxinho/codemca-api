class Api::V1::UsuariosController < ApplicationController
  respond_to :json

  def show
    respond_with Usuario.find(params[:id])
  end

  def create
    usuario = Usuario.new(usuario_params)
    if usuario.save
      render json: usuario, status: 201, location: [:api, usuario]
    else
      render json: { errors: usuario.errors }, status: 422
    end
  end

  def update
    usuario = Usuario.find(params[:id])
    if usuario.update(usuario_params)
      render json: usuario, status: 200, location: [:api, usuario]
    else
      render json: { errors: usuario.errors }, status: 422
    end
  end

  def destroy
    usuario = Usuario.find(params[:id])
    usuario.destroy
    head 204
  end

  private
  
  def usuario_params
    params.require(:usuario).permit(:email, :password, :password_confirmation)
  end

end