class Api::V1::SessionsController < ApplicationController
  def create
    usuario_password = params[:session][:password]
    usuario_email    = params[:session][:email]
    usuario          = usuario_email.present? && Usuario.find_by(email: usuario_email)
   
    if usuario.valid_password? usuario_password
      sign_in usuario, store: false
      usuario.generate_authentication_token!
      usuario.save
      render json: usuario, status: 200, location: [:api, usuario]
    else
      render json: { errors: "Invalid email or password" }, status: 422
    end
  end
end
