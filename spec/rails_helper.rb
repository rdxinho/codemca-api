require 'spec_helper'
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
abort("The Rails environment is running in production mode!") if Rails.env.production?
require 'rspec/rails'
require 'support/factory_bot'
require 'support/shoulda_matchers'
Dir[Rails.root.join('spec/support/**/*.rb')].each { |file| require file }

ActiveRecord::Migration.maintain_test_schema!

RSpec.configure do |config|
  config.fixture_path = "#{::Rails.root}/spec/fixtures"
  config.use_transactional_fixtures = true
  config.infer_spec_type_from_file_location!
  config.filter_rails_from_backtrace!
  config.include Devise::Test::ControllerHelpers, type: :controller
  config.include Devise::TestHelpers, type: :controller
  config.include RequestHelper::JsonHelpers, type: :controller
  config.include RequestHelper::HeadersHelpers, type: :controller
  config.before(:each, type: :controller) do
    include_default_accept_headers
  end
  config.mock_with :rspec do |mocks|
    mocks.syntax = [:should, :receive]
  end
end
