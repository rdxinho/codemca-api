describe Authenticable do
  describe "#authenticate_with_token" do
    before do
      @user = create(:usuario)
      authentication.stub(:current_usuario).and_return(nil)
      response.stub(:response_code).and_return(401)
      response.stub(:response).and_return(response)
    end

    it "render a json error message" do
      expect(json_response[:errors]).to eql "Not authenticated."
    end

    it { should respond_with 401 }
  end
end