require 'rails_helper'

RSpec.describe Api::V1::UsuariosController, type: :controller do

  describe 'GET #show' do
    before(:each) do  
      @usuario = create(:usuario)
      get :show, params: { id: @usuario.id }, format: :json
    end

    it "returns the information about a reporter on a hash" do
      usuario_response = json_response
      expect(usuario_response[:email]).to eql @usuario.email
    end

    it { expect(response).to have_http_status(200) }
  end

  describe 'POST #create' do
    context 'when is successfully created' do
      before(:each) do
        @usuario_attributes = attributes_for(:usuario)
        post :create, params: { usuario: @usuario_attributes }, format: :json
      end

      it "renders the json representation for the user record just created" do
        usuario_response = json_response
        expect(usuario_response[:email]).to eql @usuario_attributes[:email]
      end

      it { should respond_with 201 }
    end

    context 'when is not created' do
      before(:each) do
        @invalid_usuario_attributes = {
          password: '12345678',
          password_confirmation: '12345678'
        }

        post :create, params: { usuario: @invalid_usuario_attributes }, format: :json
      end

      it 'renders the json errors on whye the user could not be created' do
        usuario_response = json_response
        expect(usuario_response[:errors][:email]).to include "can't be blank"
      end

      it { should respond_with 422 }

    end
  end

  describe "PUT/PATCH #update" do
    before(:each) do
      @usuario = create(:usuario)
    end

    context 'when is successfully updated' do
      before(:each) do
        patch :update, params: { id: @usuario.id, usuario: {
        email: 'newemail@example.com'  }  }, format: :json
      end

      it "renders the json representation for the updated user" do
        usuario_response = json_response
        expect(usuario_response[:email]).to eql "newemail@example.com"
      end

      it { should respond_with 200 }
    end
  
    context 'when is not created' do
      before(:each) do
        patch :update, params: { id: @usuario.id, usuario: { email: "bademail.com" } }, format: :json
      end

      it 'renders an errors json' do
        usuario_response = json_response
        expect(usuario_response).to have_key(:errors)
      end

      it 'renders the json errors on whye the user could not be created.' do
        usuario_response = json_response
        expect(usuario_response[:errors][:email]).to include "is invalid"
      end

      it { should respond_with 422 }
    end
  end

  describe 'DELETE #destroy' do
   before(:each) do
    @usuario = create(:usuario)
    delete :destroy, params: { id: @usuario.id }, format: :json
   end

   it { should respond_with 204 }

  end
end