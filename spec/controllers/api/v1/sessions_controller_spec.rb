require 'rails_helper'

RSpec.describe Api::V1::SessionsController, type: :controller do
  describe "POST #create" do
    before(:each) do
      @usuario = create(:usuario)
    end

    context "when the crendetials are correct" do
      before(:each) do
        credentials = { email: @usuario.email, password: '123456' }
        post :create, params: { session: credentials }
      end

      it "returns the user record corresponding to the given credentials" do
        @usuario.reload
        expect(json_response[:auth_token]).to eql @usuario.auth_token
      end

      it { should respond_with 200 }

    end

    context "when the credentials are incorret" do
      before(:each) do
        credentials = { email: @usuario.email, password: "invalidpassword" }
        post :create, params: { session: credentials }
      end

      it "return a json with an error" do
        expect(json_response[:errors]).to eql "Invalid email or password"
      end

      it { should respond_with 422 }

    end

  end
end
