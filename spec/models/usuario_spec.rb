require 'rails_helper'

RSpec.describe Usuario, type: :model do
  before { @usuario = build(:usuario) }

  subject { @usuario }

  it { should respond_to(:auth_token) }
  it { expect(subject).to respond_to(:email) }
  it { expect(subject).to respond_to(:password) }
  it { expect(subject).to respond_to(:password) }
  pending { expect(subject).to respond_to(nome) }
  it { expect(subject).to be_valid }
  
  describe "validations" do
    it { should validate_presence_of(:email) }
    it { should validate_uniqueness_of(:email).ignoring_case_sensitivity }
    it { should validate_confirmation_of(:password) }
    it { should allow_value('example@domain.com').for(:email) }
    it { should validate_uniqueness_of(:auth_token) }
  end

  describe "#generate_authentication_token!" do
    it "generates a unique token" do
      Devise.stub(:friendly_token).and_return("auniquetoken123")
      @usuario.generate_authentication_token!
      expect(@usuario.auth_token).to eql "auniquetoken123"
    end

    it "generates another token when one already has been taken" do
      existing_usuario = create(:usuario, auth_token: "auniquetoken123" )
      @usuario.generate_authentication_token!
      expect(@usuario.auth_token).not_to eql existing_usuario.auth_token 
    end
  end
end
