require 'rails_helper'

describe ApiConstraints do
  it "returns true when the version matches the 'Accept' header" do
    request = double(host: 'api.codemca',
                           headers: { "Accept" => "application/vnd.codemca.v1" } )
    expect(api_constraints_v1.matches?(request)).to be true
  end

  it "returns the default version when 'default' options is specified" do
    request = double(host: 'api.codemca')
    expect(api_constraints_v2.matches?(request)).to be true
  end
end