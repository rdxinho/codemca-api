FROM ruby:2.4
ENV http_proxy "http://10.131.188.1:80" 
ENV https_proxy "http://10.131.188.1:80" 
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN git clone https://github.com/rodolfopeixoto/prax.git /opt/prax
WORKDIR /opt/prax
RUN ./bin/prax install
RUN cd ..
RUN mkdir /codemcaAPI
WORKDIR /codemcaAPI
RUN prax link
ADD Gemfile /codemcaAPI/Gemfile
ADD Gemfile.lock /codemcaAPI/Gemfile.lock
RUN bundle install
ADD . /codemcaAPI