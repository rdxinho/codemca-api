class AddAuthenticationTokenToUsuarios < ActiveRecord::Migration[5.2]
  def change
    add_column :usuarios, :auth_token, :string, default: ''
    add_index :usuarios, :auth_token, unique: true
  end
end
